// import 'bootstrap/dist/css/bootstrap.css';
import './index.css';

import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/app';
import dt from '../static/data/data.json';
import 'react-app-polyfill/ie11';
import 'react-app-polyfill/stable';

function ready() {
	const root = document.getElementById('ig-infographic');
	root.setAttribute('data-ua', navigator.userAgent);
	ReactDOM.render(<App data={dt} />, root);

	window.onload = function() {};

}

document.addEventListener("DOMContentLoaded", ready);