let data = [
  {
    id: 1,
    name: "George HW Bush",
    start: "20.01.1989",
    end: "20.01.1993",
    days: 1461,
    pointList: [
      {
        id: 1,
        date: '01.07.1990',
        title: '•	July 1990 ',
        body: 'Recession starts. Markets crash.',
      },{
        id: 2,
        date: '02.08.1990',
        title: '•	2nd August 1990',
        body: 'Gulf War starts. - Kuwait oil exports cut until 1994.',
      },{
        id: 3,
        date: '03.08.1990',
        title: '•	3rd August 1990',
        body: 'The Dow Jones drops 54.95 points as a result of Iraq invading Kuwait.',
      },{
        id: 4,
        date: '01.10.1990',
        title: '•	October 1990',
        body: 'Oil hits an all-time high of $41.90 a barrel.',
      },{
        id: 5,
        date: '17.01.1991',
        title: '•	17th January 1991',
        body: 'USA joins Gulf War.',
      },{
        id: 6,
        date: '28.02.1991',
        title: '•	28th February 1991',
        body: 'Gulf War ends.',
      },{
        id: 7,
        date: '30.03.1991',
        title: '•	March 1991',
        body: 'Recession ends.',
      }
    ],
  },{
    id: 2,
    name: "William J Clinton",
    start: "20.01.1993",
    end: "20.01.2001",
    days: 2922,
    pointList: [
      {
        id: 1,
        date: '10.08.1993',
        title: '•	10th August 1993 ',
        body: 'President Clinton signs the Omnibus Budget Reconciliation Act of 1993.',
      },{
        id: 2,
        date: '17.07.1995',
        title: '•	17th July 1995',
        body: 'The NASDAQ Composite closes above 1,000 for the first time.',
      },{
        id: 3,
        date: '20.01.1997',
        title: '•	20th January 1997',
        body: `Bill Clinton starts second term.`,
      },{
        id: 4,
        date: '02.02.1998',
        title: '•	2nd February 1998',
        body: 'The S&P 500 closes above 1,000 for the first time.',
      },{
        id: 5,
        date: '29.03.1999',
        title: '•	29th March 1999',
        body: 'The Dow Jones Industrial closes above 10,000 for the first time.',
      },{
        id: 6,
        date: '03.11.1999',
        title: '•	3rd November 1999 ',
        body: 'The NASDAQ Composite closes above 3,000 for the first time.',
      },{
        id: 7,
        date: '01.11.1999',
        title: '•	1st November 1999 ',
        body: 'Microsoft Corporation and Intel Corporation are the first NASDAQ-listed stocks to join the Dow Jones.',
      },{
        id: 8,
        date: '15.03.2000',
        title: '•	March 2000',
        body: `Dotcom bubble begins to burst.`,
      },{
        id: 9,
        date: '01.12.2000',
        title: '•	1st December 2000 ',
        body: 'Oil price starts to drop as Iraq stops oil exports because of dispute with UN.',
      }
    ],
  },{
    id: 3,
    name: "George W Bush",
    start: "20.01.2001",
    end: "20.01.2009",
    days: 2922,
    pointList: [
      {
        id: 1,
        date: '01.09.2001',
        title: '•	11th September 2001 ',
        body: 'Terrorist attack on US soil. Stock markets close for 4 days.',
      },{
        id: 2,
        date: '20.10.2001',
        title: '•	7th October 2001 ',
        body: 'America invades Afghanistan. Oil price continues to rise over the next 7 years.',
      },{
        id: 3,
        date: '09.10.2002',
        title: '•	9th October 2002 ',
        body: 'NASDAQ Composite closes at 1,114, its lowest since the dotcom crash.',
      },{
        id: 4,
        date: '19.03.2003',
        title: '•	19th March 2003 ',
        body: 'America invades Iraq.',
      },{
        id: 5,
        date: '20.01.2005',
        title: '•	20th January 2005',
        body: `Bush starts second term.`,
      },{
        id: 6,
        date: '19.07.2007',
        title: '•	19th July 2007 ',
        body: 'The Dow Jones hits a record high, closing above 14,000.',
      },{
        id: 7,
        date: '09.10.2007',
        title: '•	9th October 2007 ',
        body: 'S&P 500 hits a record high, closing at 1,565.15.',
      },{
        id: 8,
        date: '01.07.2008',
        title: '•	11th July 2008 ',
        body: 'Oil price hits an all-time high of $147.02.',
      },{
        id: 9,
        date: '15.08.2008',
        title: '•	15th September 2008 ',
        body: 'NASDAQ Composite drops by 3.6%, its worst single-session percentage fall since 24th March 2003.',
      },{
        id: 10,
        date: '01.10.2008',
        title: '•	29th September 2008 ',
        body: 'The Dow Jones records its worst daily point loss. ',
      }
    ],
  },{
    id: 4,
    name: "Barack H Obama",
    start: "20.01.2009",
    end: "20.01.2017",
    days: 2922,
    pointList: [
      {
        id: 1,
        date: '01.12.2008',
        title: '•	December 2008 ',
        body: 'US government begins bailout of automotive industry.',
      },{
        id: 2,
        date: '06.05.2010',
        title: '•	6th May 2010',
        body: `’The Flash Crash’: Navinder Sarao crashes the markets.
        The Dow Jones falls 1,010.14 but recovers by the end of the day.`,
      },{
        id: 3,
        date: '20.01.2013',
        title: '•	20th January 2013 ',
        body: `Obama starts second term.`,
      },{
        id: 4,
        date: '28.03.2013',
        title: '•	28th March 2013 ',
        body: 'The S&P 500 closes at a record high of above 1,569.',
      },{
        id: 5,
        date: '01.06.2014',
        title: '•	June 2014 ',
        body: 'Oil prices begin to fall rapidly because of excess production.',
      },{
        id: 6,
        date: '01.12.2014',
        title: '•	December 2014 ',
        body: 'Bailout of automotive industry ends. In total, it received nearly $81bn of taxpayers’ money.',
      },{
        id: 7,
        date: '23.04.2015',
        title: '•	23rd April 2015 ',
        body: 'The NASDAQ Composite sets a record closing high.',
      },{
        id: 8,
        date: '19.01.2016',
        title: '•	19th January 2016 ',
        body: 'Oil hits a 13-year low of $26.64 per barrel.',
      }
    ],
  },{
    id: 5,
    name: "Donald J Trump",
    start: "20.01.2017",
    end: "20.01.2021",
    // days: 1349,
    days: 1320,
    pointList: [
      {
        id: 1,
        date: '16.08.2017',
        title: '•	16th August 2017 ',
        body: 'Trump administration begins renegotiating the North American Free Trade Deal.',
      },{
        id: 2,
        date: '04.04.2018',
        title: '•	3rd & 5th April 2018 ',
        body: 'Trump announces tariffs on Chinese imports.',
      },{
        id: 3,
        date: '02.08.2018',
        title: '•	2nd August 2018 ',
        body: 'Apple becomes the first company in the world to be valued at $1tr.',
      },{
        id: 4,
        date: '24.08.2019',
        title: '•	24th August 2019 ',
        body: 'Trump tweets about China. The Dow Jones Industrial drops by 2.4%. The S&P 500 drops by 2.6% and the NASDAQ Composite falls by 3%.',
      },{
        id: 5,
        date: '16.03.2020',
        title: '•	16th March 2020 ',
        body: 'Anxiety over the coronavirus pandemic sees the Dow Jones and the S&P suffer their worst daily declines since 1987.',
      },{
        id: 6,
        date: '20.04.2020',
        title: '•	20th April 2020 ',
        body: 'For the first time in history the price of oil turns negative to - $36.98 per barrel.',
      },{
        id: 7,
        date: '02.09.2020',
        title: '•	2nd September 2020 ',
        body: 'The S&P 500 and by the NASDAQ Composite close at record highs and the Dow Jones jumps 455 points.',
      }
    ],
  },
];

const arr_HW_Bush = [
  {
    id: 1,
    date: '01.01.1990',
    title: '',
    body: '',
  },{
    id: 2,
    date: '01.01.1991',
    title: '',
    body: '',
  },{
    id: 3,
    date: '01.01.1992',
    title: '',
    body: '',
  },{
    id: 4,
    date: '01.01.1993',
    title: '',
    body: '',
  },
];

const arr_Clinton = [
  {
    id: 1,
    date: '01.01.1994',
    title: '',
    body: '',
  },{
    id: 2,
    date: '01.01.1996',
    title: '',
    body: '',
  },{
    id: 3,
    date: '01.01.1998',
    title: '',
    body: '',
  },{
    id: 4,
    date: '01.01.2000',
    title: '',
    body: '',
  },
];
const arr_W_Bush = [
  {
    id: 1,
    date: '01.01.2002',
    title: '',
    body: '',
  },{
    id: 2,
    date: '01.01.2004',
    title: '',
    body: '',
  },{
    id: 3,
    date: '01.01.2006',
    title: '',
    body: '',
  },{
    id: 4,
    date: '01.01.2008',
    title: '',
    body: '',
  },
]

const arr_Obama = [
  {
    id: 1,
    date: '01.01.2010',
    title: '',
    body: '',
  },{
    id: 2,
    date: '01.01.2012',
    title: '',
    body: '',
  },{
    id: 3,
    date: '01.01.2014',
    title: '',
    body: '',
  },{
    id: 4,
    date: '01.01.2016',
    title: '',
    body: '',
  },
];

const arr_Trump = [
  {
    id: 1,
    date: '01.01.2018',
    title: '',
    body: '',
  },{
    id: 2,
    date: '01.01.2019',
    title: '',
    body: '',
  },{
    id: 3,
    date: '01.01.2020',
    title: '',
    body: '',
  },
]

// -- points debug
// data[0].pointList = arr_HW_Bush;
// data[1].pointList = arr_Clinton;
// data[2].pointList = arr_W_Bush;
// data[3].pointList = arr_Obama;
// data[4].pointList = arr_Trump;
// ------------

export { data };