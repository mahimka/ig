import React, { Fragment } from 'react'
import html from 'react-inner-html';
import { Waypoint } from 'react-waypoint';
import { gsap } from "gsap/dist/gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import {EasePack, ScrollToPlugin} from 'gsap/all'
import Star from '../../assets/star.svg'

import Graph from '../graph'

export default class Pres extends React.Component {
  constructor(props) {
    super(props);
    gsap.defaults({overwrite: "true"});
    gsap.registerPlugin(ScrollTrigger);
    this.state={
    }
  }

  componentDidMount() {

    let startX = -100;
    if(this.props.dataid === 2 || this.props.dataid === 4)
      startX = 100;

      const tl = gsap.timeline({
        scrollTrigger: {
          trigger: `.ig-pres[data-id="${this.props.dataid}"]`,
          start: "top bottom",
          end: "bottom top",
          scrub: true,
          markers: false
        }
      });
      tl.set(`.ig-pres-img[data-id="${this.props.dataid}"]`, {x: startX, y: 0})
      tl.to(`.ig-pres-img[data-id="${this.props.dataid}"]`, {x: 0, y: 0, duration: 1})



      const tl2 = gsap.timeline({
        scrollTrigger: {
          trigger: `.ig-pres[data-id="${this.props.dataid}"]`,
          start: "top bottom",
          end: "bottom top",
          scrub: true,
          markers: false
        }
      });
      tl2.set(`.ig-pres-container[data-id="${this.props.dataid}"]`, {y: 0})
      tl2.to(`.ig-pres-container[data-id="${this.props.dataid}"]`, {y: 0, duration: 1})


  }

  handleWaypointEnter1(props){
    gsap.killTweensOf([`.ig-pres-star[data-id="${this.props.dataid}"]`]);
    gsap.set(`.ig-pres-star[data-id="${this.props.dataid}"]`, {scale:0, transformOrigin: `50% 50%`})
    gsap.to(`.ig-pres-star[data-id="${this.props.dataid}"]`, {scale: 1, duration: 0.5, stagger: 0.1, ease: "back.out(3)" })
  }
  handleWaypointLeave1(props){
    gsap.killTweensOf([`.ig-pres-star[data-id="${this.props.dataid}"]`]);
    gsap.to(`.ig-pres-star[data-id="${this.props.dataid}"]`, {scale: 0, duration: 0.2, ease: "power2.out" })
  }

  handleWaypointEnter(props){

  }
  handleWaypointLeave(props){

  }

  render() {
    const DATA = this.props.data;
    const ID = this.props.dataid;
    return (

      <Waypoint scrollableAncestor={window} bottomOffset={"30%"} fireOnRapidScroll={false}
        onEnter={(props) => {this.handleWaypointEnter(props)}}
        onLeave={(props) => {this.handleWaypointLeave(props)}}
      >

        <section className={`ig-section ig-pres`} data-id={ID}>
          <div className={`ig-pres-bg-color`} data-id={ID} />
          <div className={`ig-pres-col`}>

            <div className={`ig-pres-bg-white`} data-id={ID} />

            <div className={`ig-pres-white-container`} data-id={ID} >
              <img className={`ig-pres-img`} src={`https://www.alphagridinfographics.com/ig/img/pres${ID}.png`} alt={``} data-id={ID} />
            </div>

            <div className={`ig-pres-container`} data-id={ID}>
              
              <div className={`ig-pres-h1`} data-id={ID}>{DATA.h1}</div>
              
              <Waypoint scrollableAncestor={window} bottomOffset={"30%"} fireOnRapidScroll={false}
                onEnter={(props) => {this.handleWaypointEnter1(props)}}
                onLeave={(props) => {this.handleWaypointLeave1(props)}}
              >
              <div className={`ig-pres-info-row`} data-id={ID}>
              {
                DATA.info.map((d,i)=>(
                  <Fragment key={i}>
                    <div 
                      className={`ig-pres-info`}
                      dangerouslySetInnerHTML={{__html: d}} 
                    />
                    <Star className={`ig-pres-star`} data-id={ID}/>
                  </Fragment>
                ))
              }
              </div>
              </Waypoint>

              <div className={`ig-pres-texts`}>
              {
                DATA.text.map((d,i)=>(
                  <div className={`ig-pres-column`} key={i}>
                  {
                    d.map((q,j)=>(
                      <p className={`ig-pres-text`} dangerouslySetInnerHTML={{__html: q}} key={i+''+j}/>
                    ))
                  }
                  </div>
                ))
              }
              </div>

              <div className={`ig-pres-graph-h1`}>{DATA.graphh1}</div>

              <div className="ig-section_note">
                  <img src='https://www.alphagridinfographics.com/ig/graphs/ig-arrow.svg' className="ig-section_note_arrow section_note_arrow_left" />
                  <span>
                  Swipe to explore
                  </span>
                  <img src='https://www.alphagridinfographics.com/ig/graphs/ig-arrow.svg' className="ig-section_note_arrow section_note_arrow_right" />
                </div>

              <div className="ig-pres-graph-line" />
              <div className={`ig-pres-graph-container`}>
                <Graph dataid={ID} />
              </div>

              <div className={`ig-pres-graph-txt`}>{DATA.graphtxt}</div>
              
            </div>
          </div>
        </section>



      </Waypoint>
    );
  }
}