import React, { Fragment } from 'react';
import html from 'react-inner-html';
import { gsap, EasePack } from 'gsap/all'

import Star from '../../assets/star.svg'

export default class Stars extends React.Component {
  constructor(props) {
    super(props);

    this.state={
    }
  }

  componentDidMount() {

  }

  handleWaypointEnter(props){

  }
  handleWaypointLeave(props){

  }


  render() {
    const DATA = this.props.data;

    return (
        <section className={`ig-stars`}>
        {
          DATA.sections.map((d,i)=>(
            (d.type==="pres")
            ?
            <div 
              className={`ig-stars-row`} 
              key={i} 
              data-id={i} 
              onClick={()=>{
                  this.props.topres(i)
              }}
            >
              <div className={`ig-stars-pres`}>{d.h1}</div>
              <Star className={`ig-stars-star`} data-id={i} />
            </div>
            :
            <Fragment key={i} />
          ))
        }
        </section>

    );
  }
}