import React, { Fragment } from 'react'
import html from 'react-inner-html';

import { gsap } from "gsap/dist/gsap";
import {EasePack, ScrollToPlugin} from 'gsap/all'
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import smoothscroll from 'smoothscroll-polyfill';

import Header from '../header'
import Footer from '../footer'
import Pres from '../pres'
import Stars from '../stars'

// import jQuery from "jquery";
// window.$ = window.jQuery = jQuery;


export default class App extends React.Component {
  constructor(props) {
   
    gsap.defaults({overwrite: "true"});
    gsap.registerPlugin(ScrollToPlugin);
    gsap.registerPlugin(ScrollTrigger);
    super(props);
    this.state = {}
  }

  componentDidMount() {
      // const tl = gsap.timeline({
      //   scrollTrigger: {
      //     trigger: ".ig-pres-link[data-id='2']",
      //     start: "top bottom",
      //     end: "bottom top",
      //     scrub: true,
      //     markers: false,
      //     onEnter: ({progress, direction, isActive}) => {
      //       console.log('enter')
      //       // $( ".ig-pres[data-id='1']" ).addClass( "ig-sticky-bottom" );
      //       // gsap.set(".ig-pres[data-id='2']",{bottom: 0, position: 'sticky'})
      //     },
      //     onEnterBack: ({progress, direction, isActive}) => {
      //       // gsap.set(".ig-pres[data-id='2']",{top: 0, bottom: 'auto'})
      //       console.log('enter-back')
      //     },
      //     onLeave: ({progress, direction, isActive}) => {
      //       // gsap.set(".ig-pres[data-id='2']",{top: 'auto', bottom: 0})
      //       console.log('leave')
      //     },
      //     onLeaveBack: ({progress, direction, isActive}) => {
      //       // gsap.set(".ig-pres[data-id='2']",{top: 'auto', bottom: 0})
      //       console.log('leave-back')
      //     }          
      //   }
      // });

    // resize
    window.addEventListener('resize', this.handleResize);
    this.handleResize();
    setTimeout(this.handleResize, 1000);

  }

  handleResize() {

    let pres1 = document.querySelector(`.ig-pres[data-id='1']`)
    let pres2 = document.querySelector(`.ig-pres[data-id='2']`)
    let pres3 = document.querySelector(`.ig-pres[data-id='3']`)
    let pres4 = document.querySelector(`.ig-pres[data-id='4']`)
    let pres5 = document.querySelector(`.ig-pres[data-id='5']`)

    let wh = window.innerHeight;
    let delta1 = wh - pres1.offsetHeight
    let delta2 = wh - pres2.offsetHeight
    let delta3 = wh - pres3.offsetHeight
    let delta4 = wh - pres4.offsetHeight
    let delta5 = wh - pres5.offsetHeight

    if(delta1 < 0)
      pres1.style.top = `${delta1}px`;
    else
      pres1.style.top = `0px`;
    
    if(delta2 < 0)
      pres2.style.top = `${delta2}px`;
    else
      pres2.style.top = `0px`;

    if(delta3 < 0)
      pres3.style.top = `${delta3}px`;
    else
      pres3.style.top = `0px`;

    if(delta4 < 0)
      pres4.style.top = `${delta4}px`;
    else
      pres4.style.top = `0px`;

    if(delta5 < 0)
      pres5.style.top = `${delta5}px`;
    else
      pres5.style.top = `0px`;

  }

  componentDidUpdate(prevProps, prevState, snapshot){

  }

  backtotop(){
    let elem = document.querySelector(`.ig-header`);
    gsap.to(window, {duration: 1, scrollTo: {y:elem, offsetY: 0}, ease: "power2.inOut"});
  }

  topres(i){
    let elem = document.querySelector(`.ig-pres-link[data-id="${i}"]`);
    gsap.to(window, {duration: 1, scrollTo: {y:elem, offsetY: 0}, ease: "power2.inOut"});
  }  

  render() {
    const data = this.props.data;
    return (
        <Fragment>
          <Stars topres={this.topres} data={this.props.data}/>
          <Header data={data.sections[0]}/>
          {
            data.sections.map((d,i)=>(
              (d.type==="pres")?
              <Fragment key={i}>
                <div className={`ig-pres-link`} data-id={i}/>
                <Pres data={d} key={i} dataid={i} />
              </Fragment>
              :
              <Fragment key={i} />
            ))
          }
          <Footer data={data.sections[6]} backtotop={this.backtotop}/>
        </Fragment>
    );
  }
}
