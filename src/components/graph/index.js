import React from 'react';
import html from 'react-inner-html';
import GraphEl from './graphEl'
import {data} from '../../assets/graphs/data.js'
import { gsap, EasePack } from 'gsap/all'
import { DateTime, Interval } from 'luxon';
import img1 from '../../assets/graphs/1.svg'
import img2 from '../../assets/graphs/2.svg'
import img3 from '../../assets/graphs/3.svg'
import img4 from '../../assets/graphs/4.svg'
import img5 from '../../assets/graphs/5.svg'

export default class Graph extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      isIE: false,
      pointList: [],
    }
  }

  componentDidMount() {
    function setPercent (obj, days, start) {
      const now = DateTime.fromFormat(start, "dd.MM.yyyy");
      const later = DateTime.fromFormat(obj.date, "dd.MM.yyyy");
      const i = Interval.fromDateTimes(now, later);

      obj.days = i.length('days')
      const persent = (obj.days / days * 100).toFixed(2)
      obj.daysPers = persent * 1

      return obj
    }

    data.map( m => {
      m.pointList.map( point => setPercent(point, m.days, m.start, m.end) )
    })

    const pointList = data[this.props.dataid - 1].pointList
    this.setState({ pointList: pointList });
    this.checkIE()
  }

  checkIE () {
    if(window.document.documentMode) {
      this.setState({ isIE: true });
    }
  }

  render() {
    // const image = `graphs/${this.props.dataid}.svg`
    let image = null
    switch (this.props.dataid) {
      case 1:
        image = img1;
        break;
      case 2:
        image = img2;
        break;
      case 3:
        image = img3;
        break;
      case 4:
        image = img4;
        break;
      case 5:
        image = img5;
        break;
      default:
        console.log( "err" );
    }

    const imageLeft = `https://www.alphagridinfographics.com/ig/graphs/${this.props.dataid}_left.svg`

    return (
        <section className={`ig-section ig-graph` + (this.state.isIE ? ' ig-graph-ie' : '')}>
          <GraphEl
            dataid={this.props.dataid}
            image={image}
            imageLeft={imageLeft}
            points={this.state.pointList}
          />

          <div className="ig-section_legend">
            <img src='https://www.alphagridinfographics.com/ig/graphs/legend.svg' className="ig-section_legend_img"  />
          </div>

        </section>
    );
  }
}