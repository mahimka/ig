import React from 'react';
import { Waypoint } from 'react-waypoint';
import { gsap } from 'gsap/all'
import PointSvg from '../../assets/graphs/point.svg'

export default class GraphEl extends React.Component {
  constructor(props) {
    super(props);

    this.state={
      currentPoint: 0,
    }
  }

  pointToggle (val) {
    this.setState({ currentPoint: val });
  };

  handleWaypointEnter(props){
    gsap.to(`.ig-section.ig-pres[data-id="${this.props.dataid}"] .sip_chart_img_anim`, {x: '100%', ease: "power2.out", duration: 3});
  }

  render() {
    const Image = this.props.image

    const Tooltip = props => {
      console.log('>>>', props)
      return (
        <div className='sip_tooltip' data-id={`${this.props.dataid}${props.id}`} >
          <div className="sip_tooltip_header">{props.title}</div>
          <div className="sip_tooltip_body" dangerouslySetInnerHTML={{__html: props.body }} />
        </div>
      )
    }

    const pointList = this.props.points.map( point => {
      let classN = point.id === this.state.currentPoint? 'sip_point_w sip_point_w__active' : 'sip_point_w'
      return (
          <div
            className={classN}
            key={point.id}
            style={{left: `${point.daysPers}%`}}
          >
            {
              // this.state.currentPoint === point.id ? <Tooltip title={point.title} body={point.body} /> : null
              this.state.currentPoint === point.id ?
                <div>
                  <Tooltip title={point.title} body={point.body} id={point.id} />
                  <div className="sip_point_line" />
                </div>
                : null
            }
            
            <PointSvg
              className='sip_point'
              key={point.id}
              onMouseEnter={() => this.pointToggle(point.id)}
              onMouseLeave={() => this.pointToggle(0)}
            />
          </div>
        )
      })


    return (
      <Waypoint scrollableAncestor={window} bottomOffset={"0%"} fireOnRapidScroll={false}
        onEnter={(props) => {this.handleWaypointEnter(props)}}
      >

      <div className="sip_chart_w2" >
      <div className="sip_chart_img_left_ww2" >
          <div className="sip_chart_header_note_mob">Tap on the hotspots to explore key dates</div>
          <img src='https://www.alphagridinfographics.com/ig/graphs/legend.svg' className="sip_chart_img_legend_mob"  />
        </div>

        <div className="sip_chart_img_left_ww" >
          <div className="sip_chart_img_left_w" style={{backgroundImage: `url(${this.props.imageLeft})`}} />
        </div>
        <div className="sip_chart_w" >
          <div className="sip_chart" >


            <div>

              <div className="sip_chart_header">
                <div className="sip_chart_header_note">Roll over the hotspots to explore key dates</div>

                <div className="sip_chart_header_line_w">
                  <div className="sip_chart_header_line_arrow sip_chart_header_line_arrow_left" />
                  <div className="sip_chart_header_line_arrow sip_chart_header_line_arrow_right" />
                  <div className="sip_chart_header_line" >
                    {pointList}
                  </div>
                </div>
              </div>

            </div>

            <div className="sip_chart_img">
              {/* <img src={this.props.image} className="sip_chart_image" /> */}
              {/* <div dangerouslySetInnerHTML={{__html: this.props.image}} /> */}
              <Image />

              <div className="sip_chart_img_anim" />
              <img src='https://www.alphagridinfographics.com/ig/graphs/legend.svg' className="sip_chart_img_legend"  />
            </div>

          </div>
        </div>
      </div>
      </Waypoint>
    )
  }
}