import React, { Fragment } from 'react';
import html from 'react-inner-html';
import { Waypoint } from 'react-waypoint';
import { gsap } from "gsap/dist/gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import {EasePack, ScrollToPlugin} from 'gsap/all'

import Usa from '../../assets/usa.svg'

export default class Footer extends React.Component {
  constructor(props) {
    super(props);
    gsap.defaults({overwrite: "true"});
    gsap.registerPlugin(ScrollTrigger);
    this.state={
      collapsed: true
    }
  }

  componentDidMount() {

      const tl = gsap.timeline({
        scrollTrigger: {
          trigger: ".ig-footer",
          start: "top bottom",
          end: "bottom top",
          scrub: true,
          markers: false
        }
      });
      tl.set(".ig-usa2", {x: 200, })
      tl.to(".ig-usa2", {x: 0, duration: 1})


      const t2 = gsap.timeline({
        scrollTrigger: {
          trigger: ".ig-footer",
          start: "top bottom",
          end: "bottom top",
          scrub: true,
          markers: false
        }
      });
      t2.set(".ig-usa3", {x: -200})
      t2.to(".ig-usa3", {x: 0, duration: 1})

  }

  handleWaypointEnter(props){
    // gsap.killTweensOf([`.ig-table-td-top`,`.ig-td-bg`]);
    // gsap.set(`.ig-table-td-top`, {color:'#002954'});
    // gsap.set(`.ig-td-bg`, {opacity:0, scale: 1});
    // gsap.to(`.ig-table-td-top`, {color:'#FFFFFF', ease: "power2.out", duration:1, stagger: 0.15});
    // gsap.to(`.ig-td-bg`, {opacity:1, scale: 1, ease: "power2.out", duration:0.5, stagger: 0.15});
    gsap.killTweensOf([`.ig-table-td-top`]);
    gsap.set(`.ig-table-td-top`, {color:'#002954'});
    gsap.to(`.ig-table-td-top`, {color:'#FFFFFF', ease: "power2.out", duration:1, stagger: 0.15});
  }
  handleWaypointLeave(props){
    gsap.killTweensOf([`.ig-table-td-top`]);
    gsap.to(`.ig-table-td-top`, {color:'#002954', ease: "power2.out", duration:0.2});
  }


  render() {
    const DATA = this.props.data;

    return (
        
      <Waypoint scrollableAncestor={window} bottomOffset={"50%"} fireOnRapidScroll={false}
        onEnter={(props) => {this.handleWaypointEnter(props)}}
        onLeave={(props) => {this.handleWaypointLeave(props)}}
      >
        <section className={`ig-section ig-footer`} data-id={6}>
          <div className={`ig-footer-bg`}>
            <div className={`ig-footer-line`} />
            <div className={`ig-footer-line`} />
            <div className={`ig-footer-line`} />
            <div className={`ig-footer-line`} />
            <div className={`ig-footer-line`} />
            <div className={`ig-footer-line`} />
            <div className={`ig-footer-line`} />
            <div className={`ig-footer-line`} />
            <div className={`ig-footer-line`} />
            <div className={`ig-footer-line`} />
          </div>
          <Usa className={`ig-usa2`} />
          <Usa className={`ig-usa3`} />
          <div className={`ig-footer-col`}>
            <div className={`ig-footer-h1`}>{DATA.h1}</div>
            <div className={`ig-table`}>
              <table align="center">
              <tbody>
                  <tr>
                  {
                    DATA.index.map((d,i)=>(
                        <td 
                          key={i}
                          data-id={i}
                          className={`ig-table-td-top`}
                           
                        >
                          {/*<div className={`ig-td-bg`} data-id={i}/>*/}
                          <span className={`ig-td-bg-span`}>{d}</span>
                        </td>
                    ))
                  }
                  </tr>
                  <Fragment>
                  {
                    DATA.data.map((d,i)=>(
                      <tr key={i} className={`ig-table-tr`}>
                        {
                        d.index.map((q,j)=>(
                          <td 
                            key={i+``+j}
                            className={`ig-table-td`}
                          >
                            <span dangerouslySetInnerHTML={{__html: q}} />
                            <div className={`ig-table-pres`} data-id={j}><span>{d.pres}</span></div>
                          </td>
                        ))
                        }
                      </tr>
                    ))
                  }
                </Fragment>
                </tbody>
              </table>
            </div>


            <div 
              className={`ig-back-btn`}
              onClick={()=>{
                  this.props.backtotop()
                }}
            ><span>{`BACK TO TOP`}</span></div>


            <div className={`ig-sources`}>
              <span className={`ig-sources-text`}>{`Sources`}</span>
              <div className={`ig-sources-row` + ((this.state.collapsed)?` ig-sources-row-collapsed`:``)}>
              {
                DATA.sources.map((d,i)=>(
                    (i<1 || i>3) ?
                    <a
                      key={i}
                      className={`ig-source-a`} 
                      href={d}
                    >{d}</a>
                    :
                    <span className={`ig-source-span`} key={i} dangerouslySetInnerHTML={{__html: d}} />
                ))
                }

              </div>

              <div className={`ig-line-btn`}>
                <div className={`ig-line-btn-hr`} />
                <div className={`ig-toggle-btn`}

                onClick={()=>{
                    this.setState({collapsed: !this.state.collapsed});
                  }}

                >
                  <span>{((this.state.collapsed)?`Click to view`:`Click to close`)}</span>
                  <svg className={`ig-arrow` + ((!this.state.collapsed)?` ig-arrow-flip`:``)} xmlns="http://www.w3.org/2000/svg" width="18.901" height="12.279" viewBox="0 0 18.901 12.279">
                    <path d="M-26.87-87.055l-8.036,8.036-8.036-8.036" transform="translate(44.357 88.47)" fill="none" stroke="#002954" strokeMiterlimit="10" strokeWidth="4"/>
                  </svg>


                </div>
              </div>


            </div>



          </div>


        </section>
      </Waypoint>
    );
  }
}