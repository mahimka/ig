import React from 'react';
import html from 'react-inner-html';
import { Waypoint } from 'react-waypoint';
import { gsap } from "gsap/dist/gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import {EasePack, ScrollToPlugin} from 'gsap/all'
import Usa from '../../assets/usa.svg'

export default class Header extends React.Component {
  constructor(props) {
    super(props);
    gsap.defaults({overwrite: "true"});
    gsap.registerPlugin(ScrollTrigger);
    this.state={
    }
  }

  componentDidMount() {

      const tl = gsap.timeline({
        scrollTrigger: {
          trigger: ".ig-header",
          start: "top top",
          end: "bottom top",
          scrub: true,
          markers: false
        }
      });
      tl.to(".ig-flag", {y: 200, duration: 1})


      let scroll = document.querySelector(`.ig-header-scroll`);
      gsap.killTweensOf(scroll);
      gsap.set(scroll, {y:-3})
      gsap.to(scroll, {y: 3, repeat:-1, yoyo: true, ease:`power0.out`, duration: 0.75});



  }


  handleWaypointEnter(props){
    gsap.killTweensOf([`.ig-flag-line-bg`]);
    gsap.set(`.ig-flag-line-bg`, {scaleY:0, transformOrigin: `50% 100%`});
    gsap.to(`.ig-flag-line-bg`, {scaleY:1, ease: "power2.out", duration:1, stagger: 0.03});
  }
  handleWaypointLeave(props){
    gsap.killTweensOf([`.ig-flag-line-bg`]);
    gsap.to(`.ig-flag-line-bg`, {scaleY:0, ease: "power2.in", duration:0.5, stagger: 0});
  }

  render() {
    const DATA = this.props.data;

    return (
      <Waypoint scrollableAncestor={window} bottomOffset={"0%"} fireOnRapidScroll={false}
        onEnter={(props) => {this.handleWaypointEnter(props)}}
        onLeave={(props) => {this.handleWaypointLeave(props)}}
      >
        <section className={`ig-section ig-header`} data-id={0}>
          <div className={`ig-header-col`}>
            <div className={`ig-header-col-left`}>
              <div className={`ig-header-blue-rect`} />
              <div className={`ig-header-h1`}>{DATA.h1}</div>
              <p className={`ig-header-p`} dangerouslySetInnerHTML={{__html: DATA.text[0]}} />
              <p className={`ig-header-p`} dangerouslySetInnerHTML={{__html: DATA.text[1]}} />
              <p className={`ig-header-p`} dangerouslySetInnerHTML={{__html: DATA.text[2]}} />
              <div className={`ig-header-scroll`}>
                <span className={`ig-header-scroll-txt`}>{`SCROLL`}</span>
                <div className={`ig-header-line`}></div>
              </div>
            </div>
            <div className={`ig-header-col-right`}>

              <div className={`ig-flag-lines`}>
                <div className={`ig-flag-line`} data-id={"0"}><div  className={`ig-flag-line-bg`} data-id={"0"}/></div>
                <div className={`ig-flag-line`} data-id={"1"}><div  className={`ig-flag-line-bg`} data-id={"1"}/></div>
                <div className={`ig-flag-line`} data-id={"2"}><div  className={`ig-flag-line-bg`} data-id={"2"}/></div>
                <div className={`ig-flag-line`} data-id={"3"}><div  className={`ig-flag-line-bg`} data-id={"3"}/></div>
                <div className={`ig-flag-line`} data-id={"4"}><div  className={`ig-flag-line-bg`} data-id={"4"}/></div>
                <div className={`ig-flag-line`} data-id={"5"}><div  className={`ig-flag-line-bg`} data-id={"5"}/></div>
                <div className={`ig-flag-line`} data-id={"6"}><div  className={`ig-flag-line-bg`} data-id={"6"}/></div>
              </div>

              <div className={`ig-flag`} >
                <Usa className={`ig-usa`} />
              </div>
              <div className={`ig-header-blue-line`} />

            </div>
          </div>
        </section>
      </Waypoint>
    );
  }
}